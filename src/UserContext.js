import React from 'react';

// React.createContext() - allows us to pass information between components without using props drilling
export const UserContext = React.createContext();

// provider - allows other component to consume/use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;
