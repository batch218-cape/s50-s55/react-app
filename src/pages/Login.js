import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
// import { useNavigate } from 'react-router-dom';
import { json, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login(props) {

    // Allows us to consume the UserContext Object and its properties to be used for validation
    const {user, setUser} = useContext(UserContext);

    // State hooks to store the value of input fields
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    //hook returns a function that lets us navigate to components
    // const navigate = useNavigate()

    // Function to stimulate redirection via form submission
    function loginUser(e) {
        // prevents page redirection via form submission
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        });

        // set the email of the authenticated user in the local storage
        // localStorage.setItem('email', email);
        // // sets rgw global user state to have properties obtained from local storage
        // setUser({
        //     email: localStorage.getItem('email')
        // })

        // Clear input fields
        setEmail("");
        setPassword1("");
        // navigate('/')

        // alert(`${email} has been verified! Welcome back!`);

    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        });
    }

    useEffect(() => {

        /*
        MINI ACTIVITY
        Create a validation to enable the submit button when all fields are populated and both password match. 
        */
       
        //Validation to enable the submit button when all fields are populated and both password match.
        if (email !== "" && password1 !== "") {
                setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [email, password1])

    return (
        (user.id !== null) ?
            <Navigate to='/courses'/>
        :
            <>
            <h1>Login</h1>
            <Form onSubmit={(e) => loginUser(e)}>

                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange= {e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password1}
                        onChange= {e => setPassword1(e.target.value)}

                        required
                    />
                </Form.Group>

                
                {
                    isActive ?
                        <Button variant="primary" type="submit" id="submitBtn">
                            Login
                        </Button>
                    :
                        <Button variant="danger" type="submit" id="submitBtn" disabled>
                            Login
                        </Button>
                }
            </Form>
            </>
    )

}