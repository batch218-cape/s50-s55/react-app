import CourseCard from  '../components/CourseCard';
// import coursesData from '../data/coursesData';

import {useEffect, useState} from 'react';

export default function Courses() {
        // State that will be used to store the courses retrieved from the database
    const [courses, setCourses] = useState([]);

// To see if the mock data was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

// The "map" method loops through the individual course subject in our array and returns a component for each course
// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using "courseProp"
    // const courses = coursesData.map(course => {
    //     return (
    //         <CourseCard key={course.id} course={course} />
    //     )
    // })

    // Retrieves the courses from the database upon initial render of "courses" component
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setCourses(data.map(course => <CourseCard key={course.id} course={course}/>))
            console.log(courses)
        });
    }, [courses])

    // Props drilling - allows us to pass info from one component to another using "props"
    // curly braces {} are used for props to signify that we are providing/ passing info
    return(
        <>
        {courses}
        </>
    )
}